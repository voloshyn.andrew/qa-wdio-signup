import { CompanyType } from "../src/types/signup-enums";
import { Generator } from "../src/utils";
import SignuperModel from "./models/SignuperModel";

const generator = Generator.getInstance();

const fixture = {
  negative: [
    {
      input: generator.generateEmpty<SignuperModel>(),
      expceted: {
        errors: [
          "First Name is empty or invalid.",
          "Last Name is empty or invalid.",
          "Please enter a valid email.",
          "Please select country.",
          "Please enter your password.",
          "Passwords do not match.",
          "This field is required",
          "This field is required",
        ],
      },
    },
    {
      input: generator.generate(SignuperModel, {
        password: "QWERTY1234567!",
        confirmation: "QWERTY1234567!",
        companyType: CompanyType.Cpa,
      }),
      expceted: {
        errors: [
          "Please select IM Type.",
          "Please enter valid IM Name.",
          "Please answer the question.",
          "Please answer the question.",
          "Please answer the question.",
          "Please answer the question.",
          "Terms not checked.",
          "Terms not checked.",
        ],
      },
    },
  ],
  positive: [
    {
      input: generator.generate(SignuperModel, {
        password: "QWERTY1234567!",
        confirmation: "QWERTY1234567!",
        terms: "1",
        policy: "1",
        companyType: CompanyType.Cpa,
      }),
      expceted: {
        errors: [],
      },
    },
  ],
};

export default fixture;
