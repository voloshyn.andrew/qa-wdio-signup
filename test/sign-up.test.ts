import { Browser, Page, chromium } from "playwright";
import ProfitSocialPo from "../src/page/ProfitSocialPo";
import fixture from "./fixture";

let browser: Browser;

jest.setTimeout(300000);

beforeAll(async () => {
  browser = await chromium.launch({
    devtools: true,
  });
});

afterAll(async () => {
  await browser.close();
});

describe("On the ProfitSocial page", () => {
  let page: Page;

  beforeEach(async () => {
    page = await browser.newPage();
  });

  afterEach(async () => {
    await page.close();
  });

  const [emptyFixture, emptyStage2Fixture] = fixture.negative;

  describe("negative signup", () => {
    it("should show errors for blank form after submit, stage#1", async () => {
      const profitSocialPage = new ProfitSocialPo(page);
      await profitSocialPage.open();
      await profitSocialPage.signUpComponent.submit();
      const errors = await profitSocialPage.signUpComponent.validate();

      expect(errors).toEqual(emptyFixture.expceted.errors);
    });

    it("should show errors for blank form after submit, stage#2", async () => {
      const profitSocialPage = new ProfitSocialPo(page);
      await profitSocialPage.open();
      await profitSocialPage.signUpComponent.signUpOnStage1(emptyStage2Fixture.input);
      await profitSocialPage.signUpComponent.submit();
      const errors = await profitSocialPage.signUpComponent.validate();

      expect(errors).toEqual(emptyStage2Fixture.expceted.errors);
    });
  });

  const [submitFixture] = fixture.positive;

  describe("positive signup", () => {
    it("should completed", async () => {
      const profitSocialPage = new ProfitSocialPo(page);
      await profitSocialPage.open();
      const postData = await profitSocialPage.signUp(submitFixture.input);

      expect(postData).toEqual(submitFixture.input);
    });

    it("should not show errors for valid data until submit, stage#1", async () => {
      const profitSocialPage = new ProfitSocialPo(page);
      await profitSocialPage.open();
      await profitSocialPage.signUpComponent.fillStage(submitFixture.input);
      const errors = await profitSocialPage.signUpComponent.validate();

      expect(errors).toEqual(submitFixture.expceted.errors);
    });

    it("should not show errors for valid data until submit, stage#2", async () => {
      const profitSocialPage = new ProfitSocialPo(page);
      await profitSocialPage.open();
      await profitSocialPage.signUpComponent.fillStage(submitFixture.input);
      await profitSocialPage.signUpComponent.submit();
      const errors = await profitSocialPage.signUpComponent.validate();

      expect(errors).toEqual(submitFixture.expceted.errors);
    });
  });
});
