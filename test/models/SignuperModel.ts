import * as faker from "faker";
import {
  CompanyType, Country, HowFoundOut, MessengerType, Revenue, TrafficType,
} from "../../src/types/signup-enums";

export default class SignuperModel {
  firstName: string;

  lastName: string;

  email: string;

  country: Country;

  password: string;

  confirmation: string;

  companyName: string;

  companyType: CompanyType;

  imType: MessengerType;

  imName: string;

  trafficType: TrafficType;

  revenue: Revenue;

  topCountries: string;

  howFoundOut: HowFoundOut;

  terms: "0" | "1";

  policy: "0" | "1";

  constructor() {
    this.firstName = faker.name.firstName();
    this.lastName = faker.name.lastName();
    this.email = faker.internet.email();
    this.country = faker.random.arrayElement(Object.values(Country));
    this.password = faker.internet.password();
    this.confirmation = faker.internet.password();
    this.companyName = faker.company.companyName();
    this.companyType = faker.random.arrayElement(Object.values(CompanyType));
    this.imType = faker.random.arrayElement(Object.values(MessengerType));
    this.imName = faker.internet.userName();
    this.trafficType = faker.random.arrayElement(Object.values(TrafficType));
    this.revenue = faker.random.arrayElement(Object.values(Revenue));
    this.topCountries = faker
      .random
      .arrayElements(Object.values(Country), faker.random.number({ min: 1, max: 5 }))
      .join();
    this.howFoundOut = faker.random.arrayElement(Object.values(HowFoundOut));
    this.terms = "0";
    this.policy = "0";
  }
}
