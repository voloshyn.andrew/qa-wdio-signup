import merge from "lodash.merge";

export default class Generator {
  private static instance: Generator;

  private constructor() {
    // prevent call by user
  }

  static getInstance() {
    if (!this.instance) {
      this.instance = new Generator();
    }
    return this.instance;
  }

  generate<T>(ModelClass: new () => T, modelMixin?: Partial<T>): T {
    return merge(new ModelClass(), modelMixin ?? {});
  }

  generateEmpty<T>(): T {
    return {} as T;
  }
}
