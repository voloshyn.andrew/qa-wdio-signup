import { Page } from "playwright";
import { WebElement } from "school-helper/lib/playwright";

const CHECKBOX_XPATH = "//span[contains(@class, 'svg-icon_checkbox')]";

export default class Checkbox {
  private rootElement: WebElement;

  private checkboxElement: WebElement;

  constructor(private page: Page, rootSelector: string) {
    this.rootElement = new WebElement(this.page, rootSelector);
    this.checkboxElement = this.rootElement.getChildElement(CHECKBOX_XPATH);
  }

  async check(newState: boolean): Promise<void> {
    if (await this.getState() === newState) return;
    await this.checkboxElement.click();
  }

  async toggle(): Promise<void> {
    await this.checkboxElement.click();
  }

  private async getState(): Promise<boolean> {
    return await this.checkboxElement.getAttribute("checked") === "true";
  }
}
