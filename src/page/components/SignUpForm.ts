import { Page, Route } from "playwright";
import { Wait } from "school-helper/lib/component";
import { WebElement } from "school-helper/lib/playwright";
import { PostData } from "../../types/sign-up";
import SignUpStage1 from "./SignUpStage1";
import SignUpStage2 from "./SignUpStage2";
import SignuperModel from "../../../test/models/SignuperModel";

const FORM_XPATH = "//*[@data-slide='signup']";

const SIGNUP_ENDPOINT = "https://profitsocial.com/api/profitsocialReg";

export default class SignUpForm {
  private activeStage: SignUpStage1 | SignUpStage2;

  private postData: PostData | null;

  private form: WebElement;

  private signUpStage1: SignUpStage1;

  private signUpStage2: SignUpStage2;

  constructor(private page: Page) {
    this.form = new WebElement(page, FORM_XPATH);
    this.signUpStage1 = new SignUpStage1(page, this.form);
    this.signUpStage2 = new SignUpStage2(page, this.form);
    this.activeStage = this.signUpStage1;
    this.postData = null;
  }

  async signUpOnStage1(data: SignuperModel) {
    await this.fillStage(data);
    await this.submit();
  }

  async signUpOnStage2(data: SignuperModel): Promise<PostData> {
    await this.fillStage(data);
    await this.handleRequest();
    await this.submit();
    await new Wait().waitForTrue(async () => !!this.postData, "Request post data");
    return this.postData;
  }

  async fillStage(data: SignuperModel) {
    await this.activeStage.fill(data);
  }

  private async handleRequest() {
    await this.page.route(SIGNUP_ENDPOINT, (route: Route) => {
      const requestData = route.request().postData();
      this.postData = requestData ? JSON.parse(requestData) : requestData;
      route.fulfill({
        contentType: "text/plain",
        status: 302,
      });
    });
  }

  async validate(): Promise<string[]> {
    await Wait.wait(500);
    return this.activeStage.validate();
  }

  async submit(): Promise<void> {
    await this.activeStage.submit();
    this.activeStage = this.signUpStage2;
  }
}
