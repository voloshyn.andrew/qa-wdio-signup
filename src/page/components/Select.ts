import { Page } from "playwright";
import { WebElement } from "school-helper/lib/playwright";

const SELECT_XPATH = "//div[@class='select__value']";
const POPUP_XPATH = "//div[@class='select__dropdown']";

export default class Select {
  private rootElement: WebElement;

  private selectElement: WebElement;

  private popup: WebElement;

  constructor(private page: Page, rootSelector: string) {
    this.rootElement = new WebElement(this.page, rootSelector);
    this.selectElement = this.rootElement.getChildElement(SELECT_XPATH);
    this.popup = this.rootElement.getChildElement(POPUP_XPATH);
  }

  async select(option: string): Promise<void> {
    await this.selectElement.click();
    await this.popup.waitForElement(4000);
    const optionElement = this.getOptionByText(option);
    return optionElement.click();
  }

  getOptionByText(option: string): WebElement {
    return this.popup.getChildElement(`//*[text()="${option}"]`);
  }
}
