import { WebElement } from "school-helper/lib/playwright";
import { Page } from "playwright";

const ACTIVE_STAGE_XPATH = "//*[contains(@class, 'form__part--visible')]";
const FORM_ITEM_XPATH = "//div[contains(@class, 'form__item')]";
const ERROR_BLOCKS_XPATH = "//p[contains(@class, 'form__error-text')]";

export default abstract class SignUpBaseStage {
  protected static STAGE_FIELDS_COUNT: number = 0;

  protected submitButton: WebElement;

  protected stageElement: WebElement;

  protected fieldTitleList: string[] = [];

  constructor(protected page: Page, protected rootElement: WebElement, submitBtnSelector: string) {
    this.stageElement = rootElement.getChildElement(ACTIVE_STAGE_XPATH);
    this.submitButton = rootElement.getChildElement(submitBtnSelector);
  }

  abstract fill(data: any): any;

  async submit(): Promise<void> {
    return this.submitButton.click();
  }

  async validate(fieldCount: number): Promise<string[]> {
    const errors: string[] = [];
    for (let index = 0; index < fieldCount; index++) {
      const fieldBlock = this.stageElement.getChildElementByIndex(FORM_ITEM_XPATH, index);
      const fieldError = fieldBlock.getChildElement(ERROR_BLOCKS_XPATH);
      if (await fieldError.isElementFound()) {
        const errorMessage = await fieldError.getInnerText();
        if (errorMessage) {
          errors.push(errorMessage);
        }
      }
    }
    return errors;
  }
}
