import { Page } from "playwright";
import { WebElement } from "school-helper/lib/playwright";
import SignuperModel from "../../../test/models/SignuperModel";
import SignUpBaseStage from "./SignUpBaseStage";
import Checkbox from "./Checkbox";
import Select from "./Select";

const SUBMIT_BTN_XPATH = "//*[contains(@class, 'signup-form__submit')]";
const IM_NAME_XPATH = "//*[@name='imName']";
const GEOS_XPATH = "//*[@name='topCountries']";

const MESSENGER_XPATH = "//*[@name='imType']/parent::div";
const TRAFFIC_TYPE_XPATH = "//*[@name='trafficType']/parent::div";
const REVENUE_XPATH = "//*[@name='revenue']/parent::div";
const KNOW_ABOUT_XPATH = "//*[@name='howFoundOut']/parent::div";
const TERMS_XPATH = "//*[@id='terms-input']/parent::div[contains(@class, 'form__terms-item')]";
const POLICY_XPATH = "//*[@id='policy-input']/parent::div[contains(@class, 'form__terms-item')]";

export default class SignUpStage2 extends SignUpBaseStage {
  private messengerSelect: Select;

  private imName: WebElement;

  private trafficTypeSelect: Select;

  private revenueSelect: Select;

  private geos: WebElement;

  private knowAboutSelect: Select;

  private termsCheckbox: Checkbox;

  private policyCheckbox: Checkbox;

  constructor(page: Page, rootElement: WebElement) {
    super(page, rootElement, SUBMIT_BTN_XPATH);

    this.imName = this.stageElement.getChildElement(IM_NAME_XPATH);
    this.geos = this.stageElement.getChildElement(GEOS_XPATH);

    this.messengerSelect = new Select(page, MESSENGER_XPATH);
    this.trafficTypeSelect = new Select(page, TRAFFIC_TYPE_XPATH);
    this.revenueSelect = new Select(page, REVENUE_XPATH);
    this.knowAboutSelect = new Select(page, KNOW_ABOUT_XPATH);
    this.termsCheckbox = new Checkbox(page, TERMS_XPATH);
    this.policyCheckbox = new Checkbox(page, POLICY_XPATH);

    SignUpStage2.STAGE_FIELDS_COUNT = 8;
  }

  async fill(data: SignuperModel): Promise<void> {
    await this.messengerSelect.select(data.imType);
    await this.imName.userType(data.imName);
    await this.trafficTypeSelect.select(data.trafficType);
    await this.revenueSelect.select(data.revenue);
    await this.geos.userType(data.topCountries);
    await this.knowAboutSelect.select(data.howFoundOut);
    await this.termsCheckbox.check(data.terms === "1");
    await this.policyCheckbox.check(data.policy === "1");
  }

  async validate(): Promise<string[]> {
    return super.validate(SignUpStage2.STAGE_FIELDS_COUNT);
  }
}
