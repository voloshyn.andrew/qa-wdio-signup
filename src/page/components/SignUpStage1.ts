import { Page } from "playwright";
import { WebElement } from "school-helper/lib/playwright";
import SignuperModel from "../../../test/models/SignuperModel";
import Select from "./Select";
import SignUpBaseStage from "./SignUpBaseStage";

const SUBMIT_BTN_XPATH = "//*[contains(@class, 'signup-form__next')]";
const FIRSTNAME_XPATH = "//*[@id='firstName']";
const LASTNAME_XPATH = "//*[@id='lastName']";
const EMAIL_XPATH = "//*[@id='email']";
const COUNTRY_XPATH = "//*[@id='country']/parent::div";
const PASSWORD_XPATH = "//*[@id='password']";
const CONFIRM_XPATH = "//*[@type='password'][@name='confirmation']";
const COMPANY_NAME_XPATH = "//*[@name='companyName']";
const COMPANY_TYPE_XPATH = "//*[@name='companyType']/parent::div";

export default class SignUpStage1 extends SignUpBaseStage {
  private firstname: WebElement;

  private lastname: WebElement;

  private email: WebElement;

  private countrySelect: Select;

  private password: WebElement;

  private confirm: WebElement;

  private companyName: WebElement;

  private companyTypeSelect: Select;

  constructor(page: Page, rootElement: WebElement) {
    super(page, rootElement, SUBMIT_BTN_XPATH);

    this.firstname = this.stageElement.getChildElement(FIRSTNAME_XPATH);
    this.lastname = this.stageElement.getChildElement(LASTNAME_XPATH);
    this.email = this.stageElement.getChildElement(EMAIL_XPATH);
    this.password = this.stageElement.getChildElement(PASSWORD_XPATH);
    this.confirm = this.stageElement.getChildElement(CONFIRM_XPATH);
    this.companyName = this.stageElement.getChildElement(COMPANY_NAME_XPATH);

    this.countrySelect = new Select(page, COUNTRY_XPATH);
    this.companyTypeSelect = new Select(page, COMPANY_TYPE_XPATH);

    SignUpStage1.STAGE_FIELDS_COUNT = 8;
  }

  async fill(data: SignuperModel): Promise<void> {
    await this.firstname.userType(data.firstName);
    await this.lastname.userType(data.lastName);
    await this.email.userType(data.email);
    await this.countrySelect.select(data.country);
    await this.password.userType(data.password);
    await this.confirm.userType(data.confirmation);
    await this.companyName.userType(data.companyName);
    await this.companyTypeSelect.select(data.companyType);
  }

  async validate(): Promise<string[]> {
    return super.validate(SignUpStage1.STAGE_FIELDS_COUNT);
  }
}
