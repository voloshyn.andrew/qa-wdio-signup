import { Page } from "playwright";
import { WebElement } from "school-helper/lib/playwright";
import { PostData } from "../types/sign-up";
import SignUpForm from "./components/SignUpForm";
import SignuperModel from "../../test/models/SignuperModel";

const PAGE_URL = "https://profitsocial.com/en/";
const SIGN_UP_BTN_XPATH = "//*[@data-slideto='signup'][contains(@class, 'button')]";

export default class ProfitSocialPo {
  private signUpButton: WebElement;

  private signUpForm: SignUpForm;

  constructor(private page: Page) {
    this.signUpButton = new WebElement(this.page, SIGN_UP_BTN_XPATH);
    this.signUpForm = new SignUpForm(page);
  }

  get signUpComponent(): SignUpForm {
    return this.signUpForm;
  }

  async open() {
    return this.page.goto(PAGE_URL);
  }

  async signUp(data: SignuperModel): Promise<PostData> {
    await this.signUpButton.click();
    await this.signUpForm.signUpOnStage1(data);
    return this.signUpForm.signUpOnStage2(data);
  }
}
